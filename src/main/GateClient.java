
package main;

import gate.Annotation;
import gate.AnnotationSet;
import gate.Corpus;
import gate.CreoleRegister;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.Node;
import gate.ProcessingResource;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import java.util.Scanner;
import java.util.HashSet;

/**
 * 
 */
public class GateClient {
    
    // corpus pipeline
    private static SerialAnalyserController annotationPipeline = null;
    
    // whether the GATE is initialised
    private static boolean isGateInitilised = false;
    
    public void run(){
        
        
        if(!isGateInitilised){
            
            // initialise GATE
            initialiseGate();            
        }        

        try {                
            // create an instance of a Document Reset processing resource
            ProcessingResource documentResetPR = (ProcessingResource) Factory.createResource("gate.creole.annotdelete.AnnotationDeletePR");

            // create an instance of a English Tokeniser processing resource
            ProcessingResource tokenizerPR = (ProcessingResource) Factory.createResource("gate.creole.tokeniser.DefaultTokeniser");

            // create an instance of a Sentence Splitter processing resource
            ProcessingResource sentenceSplitterPR = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter");
            
            ProcessingResource annieGazetteerPR = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer");
            
            // locate the JAPE grammar file
            File japeOrigFile = new File("E:/Skola/DDW/jape-example.jape");
            java.net.URI japeURI = japeOrigFile.toURI();
            
            // create feature map for the transducer
            FeatureMap transducerFeatureMap = Factory.newFeatureMap();
            try {
                // set the grammar location
                transducerFeatureMap.put("grammarURL", japeURI.toURL());
                // set the grammar encoding
                transducerFeatureMap.put("encoding", "UTF-8");
            } catch (MalformedURLException e) {
                System.out.println("Malformed URL of JAPE grammar");
                System.out.println(e.toString());
            }
            
            // create an instance of a JAPE Transducer processing resource
            ProcessingResource japeTransducerPR = (ProcessingResource) Factory.createResource("gate.creole.Transducer", transducerFeatureMap);

            // create corpus pipeline
            annotationPipeline = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController");

            // add the processing resources (modules) to the pipeline
            annotationPipeline.add(documentResetPR);
            annotationPipeline.add(tokenizerPR);
            annotationPipeline.add(sentenceSplitterPR);
            annotationPipeline.add(annieGazetteerPR);
            annotationPipeline.add(japeTransducerPR);
            
            System.out.println("Enter one-word terms or terms with _ instead of spaces, if the term cannot be found, expect 404 exception");
            System.out.println("First term: ");
            Scanner s = new Scanner(System.in);
            String str = s.next();
            
            System.out.println("Second term: ");
            String str2 = s.next();
            
            
            System.out.println("Similarity according to: 1-LOCATION, 2-DATE, 3-ORGANIZATION ");
            Integer selected = s.nextInt();

            
                org.jsoup.nodes.Document wdoc = Jsoup.connect("http://en.wikipedia.org/wiki/" + str).get();
                org.jsoup.nodes.Document wdoc2 = Jsoup.connect("http://en.wikipedia.org/wiki/" + str2).get();
                wdoc = Jsoup.parse(wdoc.toString());
                wdoc2 = Jsoup.parse(wdoc2.toString());
                Document document = Factory.newDocument(wdoc.body().text().toString());
                Document document2 = Factory.newDocument(wdoc2.body().text().toString());
               // System.out.println(wdoc.text());
            
            
            
            // create a corpus and add the documents to be compared
            Corpus corpus = Factory.newCorpus("");
            corpus.add(document);
            corpus.add(document2);
            
            ArrayList<ArrayList<String>> docslist = new ArrayList<ArrayList<String>>();
            docslist.add(new ArrayList<String>());
            docslist.add(new ArrayList<String>());
            
            // set the corpus to the pipeline
            annotationPipeline.setCorpus(corpus);

            //run the pipeline
            annotationPipeline.execute();

            // LOCATION, TIME+DATE+YEAR, ORGANIZATION, 
            // loop through the documents in the corpus
            for(int i=0; i< corpus.size(); i++){

                System.out.println("============================================");
                Document doc = corpus.get(i);

                // get the default annotation set
                AnnotationSet as_default = doc.getAnnotations();
                AnnotationSet annSetTokens = as_default;

                FeatureMap futureMap = null;
                
              if (selected == 1)
              {
                  System.out.println("Location selected.");
                  annSetTokens = as_default.get("Location",futureMap);
              }
              else if (selected == 2)
              {
                  System.out.println("Date/time selected.");
                  annSetTokens = as_default.get("Date",futureMap);
                  annSetTokens.get("Time", futureMap);
              }
              else if (selected == 3)
              {
                  System.out.println("Organization selected.");
                  annSetTokens = as_default.get("Organization",futureMap);
              }
              
                  
                System.out.println("Number of annotations found: " + annSetTokens.size());

              //  ArrayList tokenAnnotations = new ArrayList(annSetTokens);
                    ArrayList tokenAnnotations = new ArrayList(annSetTokens);
                // looop through the Token annotations
                for(int j = 0; j < tokenAnnotations.size(); ++j) {

                    // get a token annotation
                    Annotation token = (Annotation)tokenAnnotations.get(j);

                    // get the underlying string for the Token
                    Node isaStart = token.getStartNode();
                    Node isaEnd = token.getEndNode();
                    String underlyingString = doc.getContent().getContent(isaStart.getOffset(), isaEnd.getOffset()).toString();
                    docslist.get(i).add(underlyingString);
                    
                    FeatureMap annFM = token.getFeatures();

                }
             
            }
            HashSet<String> common = new HashSet<String>();
            Integer res = measureSimilarity(docslist.get(0), docslist.get(1), common);
            System.out.println("Number of pairs of common annotations (similarity value): " + res);
            System.out.println("Common annotations: " + common.toString());

        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (Exception e)
        {
            System.out.println(e.toString());
        }
    }

    private Integer measureSimilarity(ArrayList<String> first, ArrayList<String> second, HashSet<String> common)
    {
        Integer res = 0;
        Integer avSize = (first.size()+second.size())/2;
        int i=0, j=0;
        for (String s : first)
        {
            for (String s2 : second)
            {
                if (s2.equals(s)) 
                {
                    res++;
                    common.add(s2);
                }
                j++;
            }
            i++;
        }
        return res/2;
    }
    
    private void initialiseGate() {
        
        try {
            // set GATE home folder
            // Eg. /Applications/GATE_Developer_7.0
            File gateHomeFile = new File("C:/Program Files (x86)/GATE_Developer_7.1");
            Gate.setGateHome(gateHomeFile);
            
            // set GATE plugins folder
            // Eg. /Applications/GATE_Developer_7.0/plugins            
            File pluginsHome = new File("C:/Program Files (x86)/GATE_Developer_7.1/plugins");
            Gate.setPluginsHome(pluginsHome);            
            
            // set user config file (optional)
            // Eg. /Applications/GATE_Developer_7.0/user.xml
            Gate.setUserConfigFile(new File("C:/Program Files (x86)/GATE_Developer_7.1", "user.xml"));            
            
            // initialise the GATE library
            Gate.init();
            
            // load ANNIE plugin
            CreoleRegister register = Gate.getCreoleRegister();
            URL annieHome = new File(pluginsHome, "ANNIE").toURL();
            register.registerDirectories(annieHome);
            
            // flag that GATE was successfuly initialised
            isGateInitilised = true;
            
        } catch (MalformedURLException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GateException ex) {
            Logger.getLogger(GateClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
}